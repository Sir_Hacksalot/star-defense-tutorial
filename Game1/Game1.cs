﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using System;

namespace Game1
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;    
        Background backgroundGfx;       
        //AnimatedSprite explosion;

        //player movement stuff
        Player player;
        int maxHorizontalSpeed = 8;
        float boardUpdateDelay = 0f; //fBoardUpdateDelay and fBoardUpdateInterval will be used to control how fast the screen can scroll overall (if we rely simply on calls to Update we can potentially get inconsistant speeds). 
        float boardUpdateInterval = 0.01f;

        //are public as they will be used with the enemy class later
        public int playAreaTop = 30;
        public int playAreaBottom = 630;

        //bullet stuff
        int bulletVerticalOffset = 12; //offset is added so bullet comes from ships cannon
        int[] bulletFacingOffsets = new int[2] { 70, 0 }; //offset bullet so it comes from the front of the ship, regardless of facing direction
        const int MAX_BULLETS = 40; //max bullets that can be on screen
        Bullet[] bullets = new Bullet[MAX_BULLETS];
        float bulletDelayTimer = 0.0f; //how long between updates
        float fireDelay = 0.1f; //how long between firing the next bullet

        //audio stuff
        const int MAX_EXPLOSION_SOUNDS = 2;
        const int MAX_BULLET_SOUNDS = 2;
        private static SoundEffect[] playerShots = new SoundEffect[MAX_BULLET_SOUNDS]; //two possible sound effects for firing, due to pickups
        private static SoundEffect[] explosionSounds = new SoundEffect[MAX_EXPLOSION_SOUNDS];
        private static SoundEffect powerPickupSound;
        //background tune
        protected static Song song;

        //enemies
        int maxEnemies = 9; //max enemies in the current wave
        int activeEnemies = 9;
        const int TOTAL_MAX_ENEMIES = 30; //max enemies on a level
        Enemy[] enemies = new Enemy[TOTAL_MAX_ENEMIES];
        Texture2D enemyShip;
        Random rndGen = new Random();
        Texture2D explosionSheet;
        Explosion[] explosions = new Explosion[TOTAL_MAX_ENEMIES + 1]; //an extra explosion for the players ship

        //title screen stuff
        int gameStarted = 0;
        Texture2D titleScreenTexture;

        //game structure (lives etc)
        int processEvents = 1; //we will use iProcessEvents to determine if we are going to continue moving enemies, bullets, and the screen. We will use this when the player crashes into an enemy to stop the action but allow the explosion animations to continue playing.
        int livesLeft = 3;
        int gameWave = 0;
        int playerScore = 0;
        float playerRespawnTimer = 4f; //the delay between the player exploding and respawning (if they have ships left)
        float playerRespawnCount = 0;


        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            this.IsFixedTimeStep = false;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            graphics.PreferredBackBufferHeight = 720;
            graphics.PreferredBackBufferWidth = 1280;
            graphics.IsFullScreen = false;
            graphics.ApplyChanges();

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            backgroundGfx = new Background(Content, @"Textures\PrimaryBackground", @"Textures\ParallaxStars");
            //backgroundGfx = new Background(Content, @"Textures\PrimaryBackground", @"Textures\PrimaryBackground1", @"Textures\ParallaxStars");
            player = new Player(Content.Load<Texture2D>(@"Textures\PlayerShip"));
            enemyShip = Content.Load<Texture2D>(@"Textures\medfighter");
            explosionSheet = Content.Load<Texture2D>(@"Textures\Explosions");
            titleScreenTexture = Content.Load<Texture2D>(@"Textures\TitleScreen");
            bullets[0] = new Bullet(Content.Load<Texture2D>(@"Textures\PlayerBullet"));

            //explosion array loading
            for (int i = 0; i < TOTAL_MAX_ENEMIES + 1; ++i)
            {
                explosions[i] = new Explosion(explosionSheet, 0, rndGen.Next(8) * 64, 64, 64, 16);
            }

            //load enemies
            for (int i = 0; i < TOTAL_MAX_ENEMIES; i++)
            {
                enemies[i] = new Enemy(enemyShip, 0, 0, 85, 85, 1);
            }

            //load sound effects
            playerShots[0] = Content.Load<SoundEffect>(@"Sounds\Scifi002");
            playerShots[1] = Content.Load<SoundEffect>(@"Sounds\Scifi050");
            explosionSounds[0] = Content.Load<SoundEffect>(@"Sounds\Battle003");
            explosionSounds[1] = Content.Load<SoundEffect>(@"Sounds\Battle004");
            powerPickupSound = Content.Load<SoundEffect>(@"Sounds\Scifi041");

            //load music
            song = Content.Load<Song>(@"Sounds\Spacebird (Ambient Mix)");
            MediaPlayer.IsRepeating = true;
            MediaPlayer.Play(song);

            //init bullets
            for (int i = 1; i < MAX_BULLETS; i++)
            {
                bullets[i] = new Bullet();
            }

        }

        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            //store values for keyboard and gamepad so we aren't querying them multiple times per update
            KeyboardState keyState = Keyboard.GetState();
            GamePadState gamepadState = GamePad.GetState(PlayerIndex.One);

            //allows the game to exit
            if (gamepadState.Buttons.Back == ButtonState.Pressed || keyState.IsKeyDown(Keys.Escape))
            {
                this.Exit();
            }

            //get elapsed time since last update
            float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;

            //if the game has started, do stuff - like update maybe???
            if (gameStarted == 1)
            {
                #region GamePlay Mode (gameStarted == 1)

                if (processEvents == 1)
                {
                    #region Processing Events (processEvents == 1)

                    //accumulate time since the last bullet was fired
                    bulletDelayTimer += elapsed;

                    //accumulate time since the players speed changed
                    player.SpeedChangeCount += elapsed;

                    //if enough time has passed that the player can change speed again, call CheckHorizontalMovementKeys
                    if (player.SpeedChangeCount > player.SpeedChangeDelay)
                    {
                        CheckHorizontalMovementKeys(keyState, gamepadState);
                    }

                    //accumulate time since the player moved vertically
                    player.VerticalChangeCount += elapsed;

                    //if enough time has passed that the player can move vertical again, call CheckVerticalMovementKeys
                    if (player.VerticalChangeCount > player.VerticalChangeDelay)
                    {
                        CheckVerticalMovementKeys(keyState, gamepadState);
                    }

                    //check other keypresses
                    CheckOtherKeys(keyState, gamepadState);

                    //update all of the active enemies and explosions
                    for (int i = 0; i < TOTAL_MAX_ENEMIES; i++)
                    {
                        if (enemies[i].IsActive)
                        {
                            enemies[i].Update(gameTime, backgroundGfx.BackgroundOffset);
                        }

                        if (explosions[i].IsActive)
                        {
                            explosions[i].Update(gameTime, backgroundGfx.BackgroundOffset); //todo - update the players explosion
                        }
                    }

                    //update the player
                    player.Update(gameTime);

                    //check for bullet hits/collisions
                    CheckBulletHits();

                    //check to see if the player has collided with any enemies
                    CheckPlayerHits();

                    //accumulate time since the board was updated
                    boardUpdateDelay += elapsed;

                    //if enough time has elapsed, update the gameboard
                    if (boardUpdateDelay > boardUpdateInterval)
                    {
                        boardUpdateDelay = 0f;
                        UpdateBoard();
                    }

                    //update the bullets
                    UpdateBullets(gameTime);

                    #endregion
                }
                else
                {
                    #region Not Processing Events (processEvents == 0)
                    
                    if (explosions[TOTAL_MAX_ENEMIES].IsActive)
                    {
                        explosions[TOTAL_MAX_ENEMIES].Update(gameTime, backgroundGfx.BackgroundOffset);
                    }

                    playerRespawnCount += elapsed;

                    if (playerRespawnCount > playerRespawnTimer)
                    {
                        livesLeft -= 1;
                        if (livesLeft > 0)
                        {
                            PlayerKilled();
                            StartNewWave();
                        }
                        else
                        {
                            gameStarted = 0;
                            processEvents = 1;
                        }
                    }
                    #endregion
                }
                #endregion
            }
            else
            {
                #region Title Screen Mode (gameStarted == 0)

                if ((keyState.IsKeyDown(Keys.Space)) || (gamepadState.Buttons.Start == ButtonState.Pressed))
                {
                    //start the game
                    StartNewGame();
                }
                #endregion
            }
            base.Update(gameTime);
        }

        protected void StartNewGame()
        {
            gameStarted = 1;
            processEvents = 1;
            livesLeft = 3;
            player.ScrollRate = 0;
            playerScore = 0;
            gameWave = 0;
            maxEnemies = 9;

            StartNewWave();
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            //clear the graphics device
            GraphicsDevice.Clear(Color.Black);

            //start the spritebatch.begin and draw stuff
            spriteBatch.Begin();

            if (gameStarted == 1)
            {
                #region Game Play Mode (gamestarted == 1)

                //draw the background object
                backgroundGfx.Draw(spriteBatch);

                //draw the players starfighter
                if (processEvents == 1)
                {
                    player.Draw(spriteBatch);

                }

                //draw any active player bullets on the screen
                for (int i = 0; i < MAX_BULLETS; i++)
                {
                    //only draw the active bullets
                    if (bullets[i].IsActive)
                    {
                        bullets[i].Draw(spriteBatch);
                    }
                }

                //draw any enemies and explosions
                for (int i = 0; i < maxEnemies; i++)
                {
                    if (enemies[i].IsActive)
                    {
                        enemies[i].Draw(spriteBatch, backgroundGfx.BackgroundOffset);
                    }

                    if (explosions[i].IsActive)
                    {
                        explosions[i].Draw(spriteBatch, false);
                    }
                }

                //draw the player explosion if required
                if (explosions[TOTAL_MAX_ENEMIES].IsActive)
                {
                    explosions[TOTAL_MAX_ENEMIES].Draw(spriteBatch, true);
                }

                #endregion
            }
            else
            {
                #region Title Screen Mode (gameStarted == 0)
                spriteBatch.Draw(titleScreenTexture, new Rectangle(0, 0, 1280, 720), Color.White);
                #endregion
            }

            //close the spritebatch
            spriteBatch.End();

            base.Draw(gameTime);
        }

        protected void CheckHorizontalMovementKeys(KeyboardState ksKeys, GamePadState gsPad)
        {
            bool resetTimer = false;

            player.Thrusting = false;
            if((ksKeys.IsKeyDown(Keys.Right)) || (gsPad.ThumbSticks.Left.X > 0) || gsPad.Triggers.Right > 0)
            {
                //if player is not travelling at max speed
                if(player.ScrollRate < maxHorizontalSpeed)
                {
                    //add to the scrollrate by the acceleration rate of the player
                    player.ScrollRate += player.AccelerationRate;
                    
                    //Correct any overspeed
                    if(player.ScrollRate > maxHorizontalSpeed)
                    {
                        player.ScrollRate = maxHorizontalSpeed;
                        resetTimer = true;
                    }
                    player.Thrusting = true;
                    player.Facing = (int)Player.PlayerDirection.RIGHT;
                }
            }

            if ((ksKeys.IsKeyDown(Keys.Left)) || (gsPad.ThumbSticks.Left.X < 0) || gsPad.Triggers.Left > 0)
            {
                //if player is not travelling at max speed
                if (player.ScrollRate > -maxHorizontalSpeed)
                {
                    //add to the scrollrate by the acceleration rate of the player
                    player.ScrollRate -= player.AccelerationRate;

                    //Correct any overspeed
                    if (player.ScrollRate < -maxHorizontalSpeed)
                    {
                        player.ScrollRate = -maxHorizontalSpeed;
                        resetTimer = true;
                    }
                    player.Thrusting = true;
                    player.Facing = (int)Player.PlayerDirection.LEFT;
                }
            }

            if (resetTimer)
            {
                player.SpeedChangeCount = 0.0f;
            }
        }

        protected void CheckVerticalMovementKeys(KeyboardState ksKeys, GamePadState gsPad)
        {
            /**
             * Just like our horizontal movement checks, we determine if a vertical movement key has been pressed and reset the player.VerticalChangeCount variable if appropriate. 
             * We add (or subtract) player.VerticalMovementRate to the player.Y position. 
             */

            bool resetTimer = false;

            if((ksKeys.IsKeyDown(Keys.Up)) || (gsPad.ThumbSticks.Left.Y > 0))
            {
                if(player.Y > playAreaTop)
                {
                    player.Y -= player.VerticalMovementRate;
                    resetTimer = true;
                }
            }

            if((ksKeys.IsKeyDown(Keys.Down))|| (gsPad.ThumbSticks.Left.Y < 0))
            {
                if(player.Y < playAreaBottom)
                {
                    player.Y += player.VerticalMovementRate;
                    resetTimer = true;
                }
            }

            if(resetTimer)
            {
                player.VerticalChangeCount = 0f;
            }
        }

        public void UpdateBoard()
        {

            //backgroundGfx.ParallaxOffset += player.ScrollRate * 2;
            //backgroundGfx.BackgroundOffset += player.ScrollRate;

            //scrolling fixes, prevents scrolling from stopping dead
            if (player.ScrollRate > -2 && player.ScrollRate < 2)
            {
                if (player.Facing == (int)Player.PlayerDirection.RIGHT)
                {
                    backgroundGfx.BackgroundOffset += 1;
                    backgroundGfx.ParallaxOffset += 2 * 2;
                }
                else
                {
                    backgroundGfx.BackgroundOffset += -1;
                    backgroundGfx.ParallaxOffset += -4;
                }

            }
            else
            {
                backgroundGfx.ParallaxOffset += player.ScrollRate * 2;
                backgroundGfx.BackgroundOffset += player.ScrollRate;
            }
        }

        protected void UpdateBullets(GameTime gameTime)
        {
            //updates the location of all the active player bullets
            /**
             * This function simply loops through all of the bullets in our array and calls the 
             * Update method for each active bullet, passing it the current GameTime.
             */
            for (int i = 0; i < MAX_BULLETS; i++)
            {
                if (bullets[i].IsActive)
                {
                    bullets[i].Update(gameTime);
                }
            }

        }

        protected void FireBullet(int verticalOffset)
        {
            //find and fire a free bullet
            for (int i = 0; i < MAX_BULLETS; i++)
            {
                if(!bullets[i].IsActive)
                {
                    bullets[i].Fire(player.X + bulletFacingOffsets[player.Facing], player.Y + bulletVerticalOffset + verticalOffset, player.Facing);
                    //play audio
                    if (verticalOffset == 0)
                    {
                        playerShots[0].Play(1.0f, 0f, 0f); //this line will need adjusting for WeaponLevel (not yet implemented)
                    }
                    break;
                    /**
                     * Since we are using an array of bullet objects, we need to find a "free" bullet to fire. 
                     * This is a bullet object that isn't currently active, so a simple loop finds the first non-active bullet,
                     * execute's it's Fire() method using the player's position and the offsets we discussed above to establish its initial position. 
                     * (As soon as we have found one, we can exit the loop, so we "break;" out of it to prevent the method from firing all the 
                     * available bullets with each button press.) You will notice that we add a second vertical offset here that is passed into our helper function. 
                     * We will use this when we add powerups, as one of the powerups will be "dual cannons" which 
                     * will fire a second bullet that is 4 pixels above the normal bullet. 
                     * If we didn't find a free bullet nothing will happen, but that would mean that every bullet is currently on the screen! 
                     * With 40 bullets and a delay between firing, this should never happen. 
                     */
                }
            }
        }

        protected void CheckOtherKeys(KeyboardState ksKeys, GamePadState gsPad)
        {
            /**
             * Spacebar or gamepad A button to fire the players weapon.
             * The weapon has its own regulating delay (bulletDelayTimer)
             * to pace the firing of the players weapon.
             */

            if ((ksKeys.IsKeyDown(Keys.Space)) || (gsPad.Buttons.A == ButtonState.Pressed))
            {
                if (bulletDelayTimer >= fireDelay)
                {
                    FireBullet(0); //no vertical offset required yet
                    bulletDelayTimer = 0.0f;
                    
                }
                /**
                 * We will call this routine from our Update method (we'll expand it later to account for SuperBombs and other Power Ups). 
                 * Either the space bar or the "A" button on the gamepad will fire the player's weapon. 
                 * We use our standard timing method to determine if a bullet can be fired. 
                 * If it can, we call FireBullet with a vertical offset of 0. 
                 */
            }


        }

        protected void GenerateEnemies()
        {
            /**
             * This helper function checks to see if maxEnemies is less than TOTAL_MAX_ENEMIES and, 
             * if it is, adds one to iMaxEnemies. Whenever this routine is called, a new wave of enemies, 
             * one enemy larger than the last, will be generated. Note that existing enemies will simply be 
             * "regenerated" if they have been destroyed. We update our activeEnemies count for each enemy we create. 
             * */

            if (maxEnemies < TOTAL_MAX_ENEMIES)
            {
                maxEnemies++;
            }

            activeEnemies = 0;

            for (int i = 0; i < maxEnemies; i++)
            {
                enemies[i].Generate(backgroundGfx.BackgroundOffset, player.X);
                activeEnemies += 1;
            }
        }
            
        protected void StartNewWave()
        {
            processEvents = 1;
            gameStarted = 1;
            gameWave++;
            GenerateEnemies();
        }

        protected void PlayerKilled()
        {
            //reset the gameWave and MaxEnemies, since they will both be bumped automatically when the new wave is generated
            gameWave--;
            maxEnemies--;
            //stop the players ship
            player.ScrollRate = 0;
        }

        protected bool Intersects(Rectangle rectA, Rectangle rectB)
        {
            //return true if rectA and rectB contain any overlapping points
            return (rectA.Right > rectB.Left && rectA.Left < rectB.Right &&
                rectA.Bottom > rectB.Top && rectA.Top < rectB.Bottom);
        }

        protected void DestroyEnemy(int enemy)
        {
            //deactivate the enemy
            enemies[enemy].deactivate();
            //activate the explosion
            explosions[enemy].Activate(
                enemies[enemy].X + 10,
                enemies[enemy].Y + 10,
                enemies[enemy].Motion,
                enemies[enemy].Speed / 2,
                enemies[enemy].Offset
                ); //i'm calling a function - yeah it looks weird
            explosionSounds[0].Play(1.0f, 0f, 0f);
            activeEnemies--;
        }

        protected void CheckPlayerHits()
        {
            for (int i = 0; i < TOTAL_MAX_ENEMIES; i++)
            {
                if (enemies[i].IsActive)
                {
                    //if the enemy and ship sprites collide
                    if (Intersects(player.BoundingBox, enemies[i].CollisionBox))
                    {
                        //stop event processing
                        processEvents = 0;

                        //setup the ships explosion
                        explosions[TOTAL_MAX_ENEMIES].Activate(
                            player.X - 16,
                            player.Y - 16,
                            Vector2.Zero,
                            0f,
                            backgroundGfx.BackgroundOffset);
                        //placeholder soundeffect
                        explosionSounds[0].Play(1.0f, 0f, 0f);

                        playerRespawnCount = 0.0f;

                        return;
                    }
                }
            }
        }

        protected void RemoveBullet(int bullet)
        {
            bullets[bullet].IsActive = false;
        }

        protected void CheckBulletHits()
        {
            //check to see if any of the players bullets have impacted any of the enemies
            for(int i = 0; i < MAX_BULLETS; i++)
            {
                if (bullets[i].IsActive)
                {
                    for (int j = 0; j < TOTAL_MAX_ENEMIES; j++)
                    {
                        if (enemies[j].IsActive)
                        {
                            if(Intersects(bullets[i].BoundingBox, enemies[j].CollisionBox))
                            {
                                DestroyEnemy(j);
                                RemoveBullet(i);
                            }

                        }
                    }
                }

                if(activeEnemies < 1)
                {
                    StartNewWave();
                }
            }
        }

    }
}
