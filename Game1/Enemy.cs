﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace Game1
{
    class Enemy
    {
        AnimatedSprite aSprite;

        static int mapWidth = 1920; //We will need to know how big the game board is when we translate from World Coordinates to Screen Coordinates. This static value (shared by all enemies) holds that size. 
        static int playAreaTop = 30; //Enemies can't move above the playAreaTop pixel mark, nor below the playAreaBottom pixel mark. 
        static int playAreaBottom = 630;
        static Random rndGen = new Random(); //generate random numbers to control our enemies movements. It is static because there is no need for a different random number generator instance for each enemy. 

        int xPos = 0;
        int yPos = -100;
        const int WIDTH = 85;
        const int HEIGHT = 85;
        int backgroundOffset = 0; //This is passed into the enemy during the Update routine and lets the enemy know how far the player has scrolled on the screen. We will need to know this when position our graphic on the screen. 
        Vector2 vecMotion = new Vector2(0f, 0f); //This Vector2 value will hold the direction that the enemy ship is currently moving in.
        float speed = 1f; //The speed at which v2motion is applied to the position of the enemy. 
        float enemyMoveCount = 0.0f; //These floats represent our standard timing functions to keep a consistant gameplay rate. 
        float enemyDelay = 0.01f;
        bool active = false;

        public int X
        {
            get { return xPos; }
            set { xPos = value; }
        }

        public int Y
        {
            get { return yPos; }
            set { yPos = value; }
        }

        public bool IsActive
        {
            get { return active; }
        }

        public Rectangle BoundingBox
        {
            get {
                int tempX = xPos - backgroundOffset;
                if (tempX > mapWidth)
                {
                    tempX -= mapWidth;
                }
                if (tempX < 0)
                {
                    tempX += mapWidth;
                }
                return new Rectangle(tempX, yPos, WIDTH, HEIGHT); //changed to 85x85 cos original texture was shit
            }
        }

        public Rectangle CollisionBox
        {
            get {
                int tempX = xPos - backgroundOffset;
                if (tempX > mapWidth)
                {
                    tempX -= mapWidth;
                }
                if(tempX < 0)
                {
                    tempX += mapWidth;
                }
                return new Rectangle(tempX + 45, yPos + 15, 20, 55);
            }
        }


        public int Offset
        {
            get { return backgroundOffset; }
            set { backgroundOffset = value; }
        }

        public float Speed
        {
            get { return speed; }
        }

        public Vector2 Motion
        {
            get { return vecMotion; }
            set { vecMotion = value; }
        }

        //constructor
        public Enemy (Texture2D texture, int x, int y, int w, int h, int frames)
        {
            aSprite = new AnimatedSprite(texture, x, y, w, h, frames);
        }

        public void deactivate()
        {
            //Deactivate deactivates an enemy. Non-active enemies won't update or draw. 
            active = false;
        }

        private int getDrawX()
        {
            /**
             * The GetDrawX() method translates the enemy's "world position" X coordinate to a screen position by subtracting the value of iBackgroundOffset 
             * from the world-X position. It then accounts for wrapping off of either end of the map by adding or subtracting the width of the map.
             * 
             */

            int X = xPos - backgroundOffset;
            
            ////Interesting chasing effect
            //int X;
            //if (xPos > backgroundOffset)
            //{
            //    X = xPos - backgroundOffset;
            //}
            //else
            //{
            //    X = xPos + backgroundOffset;
            //}

            if (X < -WIDTH)
            {
                X += mapWidth;
            }

            if (X > mapWidth)
            {
                X -= mapWidth;
            }
            


            return X;
        }

        public void RandomizeMovement()
        {
            vecMotion.X = rndGen.Next(-50, 50); //generate random X and Y values
            vecMotion.Y = rndGen.Next(-50, 50);
            vecMotion.Normalize(); //turn the vector into a unit vector with a length of 1 (direction information but no speed)
            speed = (float)(rndGen.Next(3, 6)); //generate random speed value between 3 and 6
        }

        public void Generate(int location, int shipX)
        {
            //generate a random X location that is not within 200px of the players ship
            /**
             * We will call Generate() from our game whenever we need to set up a new enemy. 
             * The Generate() method will randomize the location of the enemy ship, using Math.Abs (absolute value) to make sure that it 
             * doesn't end up within 200 pixels of the player's ship horizontally. 
             * */
            do
            {
                backgroundOffset = location;
                xPos = rndGen.Next(mapWidth);
            } while (Math.Abs(getDrawX() - shipX) < 200);
            
            //generate a random Y location between playAreaTop and playAreaBottom
            /**
             * The vertical location of the ship is generated beteween the bounds of the Play Area, 
             * and our RandomizeMovement() method is called to establish an initial direction and speed for our enemy. 
             * Finally, the enemy is made active by setting bActive to true. 
             * */
            yPos = rndGen.Next(playAreaTop, playAreaBottom);
            RandomizeMovement();
            active = true;
        }

        public void Draw(SpriteBatch spriteBatch, int location)
        {
            if (active)
            {
                aSprite.Draw(spriteBatch, getDrawX(), yPos, false);
            }
        }

        public void Update(GameTime gameTime, int offset)
        {
            backgroundOffset = offset;

            enemyMoveCount += (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (enemyMoveCount > enemyDelay)
            {
                //update the enemy position based on the v2motion vector and fSpeed
                xPos += (int)((float)vecMotion.X * speed);
                yPos += (int)((float)vecMotion.Y * speed);

                if (rndGen.Next(200) == 1)
                {
                    // generate a random number between 0 and 199. 
                    //If the result is a 1 (so 0.5% chance) we will generate a new random movement for our enemy. 
                    RandomizeMovement();
                }

                //if we have moved off of the top or bottom of the play area. 
                //If we have, we clamp back to the play area and generate a new random movement.
                if (yPos < playAreaTop)
                {
                    yPos = playAreaTop;
                    RandomizeMovement();
                }

                if (yPos > playAreaBottom)
                {
                    yPos = playAreaBottom;
                    RandomizeMovement();
                }


                //If our X position moves off of either side of the game map 
                //(ie, becomes less than 0 or greater than the map width) we add or subtract the map width as appropriate to wrap it around. 

                if (xPos < -WIDTH)  //allow for sprite width
                {
                    xPos += mapWidth;
                }

                if (xPos > mapWidth)
                {
                    xPos -= mapWidth;
                }

                enemyMoveCount = 0f; //reset the movecount

            }
            aSprite.Update(gameTime);
        }

        

    }
}
