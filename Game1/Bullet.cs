﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game1
{
    class Bullet
    {
        static AnimatedSprite bulletLeft;
        static AnimatedSprite bulletRight;

        int xPos;
        int yPos;
        Vector2 bulletVector; //wip - not used yet. Will replace int X and Y
        bool active;
        int facing = 0; 
        float elapsed = 0f;
        float updateInterval = 0.015f;
        public int speed = 12;

        public enum BulletDirection
        {
            RIGHT = 0,
            LEFT = 1
        };

        public int X
        {
            get { return xPos; }
            set { xPos = value; }
        }
        public int Y
        {
            get { return yPos; }
            set { yPos = value; }
        }
        public bool IsActive
        {
            get { return active; }
            set { active = value; }
        }

        public int Facing
        {
            get { return facing; }
            set { facing = value; }
        }

        public int Speed
        {
            get { return speed; }
            set { speed = value; }
        }

        public Rectangle BoundingBox
        {
            get { return new Rectangle(xPos, yPos, 16, 1); }
        }

        //constructor
        public Bullet(Texture2D texture)
        {
            bulletRight = new AnimatedSprite(texture, 0, 0, 16, 1, 1);
            bulletLeft = new AnimatedSprite(texture, 16, 0, 16, 1, 1);
            bulletRight.IsAnimating = false;
            bulletLeft.IsAnimating = false;
            facing = (int)BulletDirection.RIGHT;
            xPos = 0;
            yPos = 0;
            active = false;
        }

        public Bullet()
        {
            facing = (int)BulletDirection.RIGHT;
            xPos = 0;
            yPos = 0;
            active = false;
        }

        public void Fire(int xPos, int yPos,int facing)
        {
            //when called, the position of the bullet is set.
            //the bullet is then made active.
            this.xPos = xPos;
            this.yPos = yPos;
            this.facing = facing;
            active = true;
        }

        public void Update(GameTime gameTime)
        {
            if (active)
            {

                elapsed += (float)gameTime.ElapsedGameTime.TotalSeconds;
                if(elapsed > updateInterval)
                {
                    elapsed = 0f;
                    if(facing == (int)BulletDirection.RIGHT)
                    {
                        xPos += speed;
                    }
                    else
                    {
                        xPos -= speed;
                    }

                    //if the bullet has moved off screen, set it to inactive
                    if(xPos > 1280 || xPos < 0)
                    {
                        active = false;
                    }
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (active)
            {
                //work out which way the bullet is facing and draw that texture
                if (facing == (int)BulletDirection.RIGHT)
                {
                    bulletRight.Draw(spriteBatch, xPos, yPos, false);
                }
                else
                {
                    bulletLeft.Draw(spriteBatch, xPos, yPos, false);
                }
            }
        }






    }
}
