﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game1
{
    class Player
    {
        public enum PlayerDirection
        {
            RIGHT = 0,
            LEFT = 1
        };

        AnimatedSprite aSprite;
        int xPos = 604; //X location of ship onscreen
        int yPos = 260; //Y location of ship onscreen
        int facing = (int)PlayerDirection.RIGHT; //direction the ship is facing 0 = Right, 1 = Left - should it be an enum? Yes it should. And now is.
        bool thrusting = false; //true when player is actively moving in a direction
        int scrollRate = 0; //determines the speed and direction. Positive values are right movement, negative is left.
        int shipAccelerationRate = 1; //how fast the scrollrate can change
        int shipVerticalMoveRate = 6; //how many pixels the ship moves in the vertical 
        float speedChangeCount = 0.0f; //time since last speed change
        float speedChangeDelay = 0.1f; //the delay between speed changes
        float verticalChangeCount = 0.0f; //similar to speedchangecount
        float verticalChangeDelay = 0.01f; //similar to speedchangedelay

        public int X
        {
            get { return xPos; }
            set { xPos = value; }
        }

        public int Y
        {
            get { return yPos; }
            set { yPos = value; }
        }

        public int Facing
        {
            get { return facing; }
            set { facing = value; }
        }
        public bool Thrusting
        {
            get { return thrusting; }
            set { thrusting = value; }
        }
        public int ScrollRate
        {
            get { return scrollRate; }
            set { scrollRate = value; }
        }
        public int AccelerationRate
        {
            get { return shipAccelerationRate; }
            set { shipAccelerationRate = value; }
        }
        public int VerticalMovementRate
        {
            get { return shipVerticalMoveRate; }    
            set { shipVerticalMoveRate = value; }
        }

        public float SpeedChangeCount
        {
            get { return speedChangeCount; }
            set { speedChangeCount = value; }
        }
        public float SpeedChangeDelay
        {
            get { return speedChangeDelay; }
            set { speedChangeDelay = value; }
        }
        public float VerticalChangeCount
        {
            get { return verticalChangeCount; }
            set { verticalChangeCount = value; }
        }
        public float VerticalChangeDelay
        {
            get { return verticalChangeDelay; }
            set { verticalChangeDelay = value; }
        }

        public Rectangle BoundingBox
        {
            get { return new Rectangle(xPos, yPos, 72, 16); }
        }

        //constructor
        /*
         * In this case, all we are doing is passing the texture along to create our AnimatedSprite. 
         * We set the frame size to 72x16 and tell the AnimatedSprite that it has 4 frames. 
         * Then we set IsAnimating to false, which will prevent the sprite from updating frames on its own. 
         * */
        public Player(Texture2D texture)
        {
            aSprite = new AnimatedSprite(texture, 0, 0, 72, 16, 4);
            aSprite.IsAnimating = false;
        }


        /*
         * Here we just pass the SpriteBatch object on to the AnimatedSprite's Draw method. 
         * We include the X and Y position of the sprite. 
         * The final parameter (false) tells the AnimatedSprite not to add SpriteBatch.Begin and SpriteBatch.End calls of it's own. 
         */
        public void Draw(SpriteBatch spriteBatch)
        {
            aSprite.Draw(spriteBatch, xPos, yPos, false);
        }

        public void Update(GameTime gameTime)
        {
            /*
             By checking the combination of iFacing and bThrusting we determine what to set the sprite's Frame value to 
             (0=Right, 1=Right with Thrust, 2=Left, 3=Left with Thrust). 
             */

            if (facing == (int)PlayerDirection.RIGHT)
            {
                if (thrusting)
                {
                    aSprite.Frame = 1;
                }
                else
                {
                    aSprite.Frame = 0;
                }
            }
            else
            {
                //player must be facing left
                if(thrusting)
                {
                    aSprite.Frame = 3;
                }
                else
                {
                    aSprite.Frame = 2;
                }
            }
        }

    }   
}       
    
    