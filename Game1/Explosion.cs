﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace Game1
{
    class Explosion
    {
        static int MAP_WIDTH = 1920; 

        AnimatedSprite aSprite;
        int xPos = 0;
        int yPos = -100;
        bool active = true; //determines if explosion is active
        int backGroundOffset = 0;
        Vector2 v2Motion = new Vector2(0f, 0f);
        float speed = 1f;

        public int X
        {
            get { return xPos; }
            set { xPos = value; }
        }

        public int Y
        {
            get { return yPos; }
            set { yPos = value; }
        }

        public bool IsActive
        {
            get { return active; }
        }

        public int Offset
        {
            get { return backGroundOffset; }
            set { backGroundOffset = value; }
        }

        public float Speed
        {
            get { return speed; }
            set { speed = value; }
        }

        public Vector2 Motion
        {
            get { return v2Motion; }
            set { v2Motion = value; }
        }

        //constructor
        public Explosion(Texture2D texture, int X, int Y, int W, int H, int Frames)
        {
            aSprite = new AnimatedSprite(texture, X, Y, W, H, Frames);
            aSprite.FrameLength = 0.05f;
        }

        public void Activate(int x, int y, Vector2 motion, float speed, int offset)
        {
            xPos = x; //positioning the explosion
            yPos = y;
            v2Motion = motion;
            this.speed = speed;
            backGroundOffset = offset;
            aSprite.Frame = 0; //starting frame
            active = true; //to be animated
        }

        private int GetDrawX()
        {
            //translate world position to screen position

            int X = xPos - backGroundOffset;
            if(X > MAP_WIDTH)
            {
                X -= MAP_WIDTH;
            }

            if(X < 0)
            {
                X += MAP_WIDTH;
            }

            return X;
        }

        public void Update(GameTime gameTime, int offset)
        {
            //pass in gametime to maintain framerate

            if (active)
            { 
                //the explosion is active, set the offset
                backGroundOffset = offset;

                //modify the explosion position
                xPos += (int)((float)v2Motion.X * speed);
                xPos += (int)((float)v2Motion.X * speed);
                aSprite.Update(gameTime); //update the sprite animation

                if(aSprite.Frame >= 15)
                {
                    //the animation is over, deactivate it
                    active = false;
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch, bool absolute)
        {
            if (active)
            {
                if(!absolute)
                {
                    //explosion of enemy ships
                    aSprite.Draw(spriteBatch, GetDrawX(), yPos, false);
                }
                else
                {
                    //for explosion of the players ship 
                    aSprite.Draw(spriteBatch, xPos, yPos, false);
                }
            }
        }


    }
}
