﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace Game1
{
    class AnimatedSprite
    {
        Texture2D texture;

        float frameRate = 0.02f; //time in seconds that each frame is displayed at
        float elapsed = 0.0f; //time since the last frame

        int frameOffsetX = 0; //co-ords for multiple sprites on the same sheet
        int frameOffsetY = 0; //co-ords for multiple sprites on the same sheet
        int frameWidth = 32; //width of individual frame
        int frameHeight = 32; //height of individual frame

        int frameCount = 1; //total number of frames in the animation
        int currentFrame = 0; //the frame that is currently getting updated
        int screenX = 0; //onscreen X co-ordinate occupied by the sprite - will be using an offset instead so these will probably be set to 0
        int screenY = 0; //onscreen Y co-ordinate occupied by the sprite - will be using an offset instead so these will probably be set to 0

        bool animating = true; //if false, frames will not be automatically updated/animated

        public AnimatedSprite(
            Texture2D texture, 
            int frameOffsetX, 
            int frameOffsetY, 
            int frameWidth, 
            int frameHeight, 
            int frameCount)
        {
            this.texture = texture;
            this.frameOffsetX = frameOffsetX;
            this.frameOffsetY = frameOffsetY;
            this.frameWidth = frameWidth;
            this.frameHeight = frameHeight;
            this.frameCount = frameCount;
        }

        public Rectangle GetSourceRect()
        {   
            return new Rectangle(
                frameOffsetX + (frameWidth * currentFrame),
                frameOffsetY,
                frameWidth,
                frameHeight);
        }

        public void Update(GameTime gameTime)
        {
            if (animating)
            {
                //accumulate elapsed time...
                elapsed += (float)gameTime.ElapsedGameTime.TotalSeconds;

                //until it passes our frame length
                if (elapsed > frameRate)
                {
                    //increment the current frame, wrapping back to 0 at frameCount
                    currentFrame = (currentFrame + 1) % frameCount;

                    //reset the elapsed frame time
                    elapsed = 0.0f;
                }
            }
        }

        public void Draw(
            SpriteBatch spriteBatch, 
            int xOffset, 
            int yOffset, 
            bool needBeginEnd)
        {
            if (needBeginEnd)
            {
                spriteBatch.Begin();
            }

            spriteBatch.Draw(
                texture,
                new Rectangle(screenX + xOffset, screenY + yOffset, frameWidth, frameHeight),
                GetSourceRect(),
                Color.White);

            if (needBeginEnd)
            {
                spriteBatch.End();
            }
        }

        public void Draw(SpriteBatch spriteBatch, int xOffset, int yOffset)
        {
            Draw(spriteBatch, xOffset, yOffset, true);
        }

        public int X
        {
            get { return screenX; }
            set { screenX = value; }
        }

        public int Y
        {
            get { return screenY; }
            set { screenY = value; }
        }

        public int Frame
        {
            get { return currentFrame; }
            set { currentFrame = (int)MathHelper.Clamp(value, 0, frameCount); }
        }

        public float FrameLength
        {
            get { return frameRate; }
            set { frameRate = (float)Math.Max(value, 0f); }
        }

        public bool IsAnimating
        {
            get { return animating; }
            set { animating = value; }
        }



    }
}
