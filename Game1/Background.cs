﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;


namespace Game1
{
    class Background
    {
        //textures to hold the 2 background images
        Texture2D background;
        Texture2D backgrounddebug; //debug image to show separation of screen wrap
        Texture2D parallax;

        //how large our background image will be displayed when rendered to the screen
        int viewportWidth = 1280;
        int viewportHeight = 720;

        int backgroundWidth = 1920;
        int backgroundHeight = 720;

        int parallaxWidth = 1680;
        int parallaxHeight = 480;


        private int backgroundOffset;

        public int BackgroundOffset
        {
            get { return backgroundOffset; }
            set
            {
                backgroundOffset = value;
                if (backgroundOffset < 0)
                {
                    backgroundOffset += backgroundWidth;
                }

                if (backgroundOffset > backgroundWidth)
                {
                    backgroundOffset -= backgroundWidth;
                }
            }
        }

        private int parallaxOffset;

        public int ParallaxOffset
        {
            get { return parallaxOffset; }
            set
            {
                parallaxOffset = value;
                if (parallaxOffset < 0)
                {
                    parallaxOffset += parallaxWidth;
                }

                if (parallaxOffset > parallaxWidth)
                {
                    parallaxOffset -= parallaxWidth;
                }
            }
        }



        //determines if we draw the parallax overlay
        private bool drawParallax = true;

        public bool DrawParallax
        {
            get { return drawParallax; }
            set { drawParallax = value; }
        }


        //constructor - scrolling background and parallax
        public Background(ContentManager content, string background, string parallax)
        {
            this.background = content.Load<Texture2D>(background);
            this.backgroundWidth = this.background.Width;
            this.backgroundHeight = this.background.Height;
            this.parallax = content.Load<Texture2D>(parallax);
            this.parallaxWidth = this.parallax.Width;
            this.parallaxHeight = this.parallax.Height;
        }

        public Background(ContentManager content, string background, string backgrounddebug, string parallax)
        {
            this.background = content.Load<Texture2D>(background);
            this.backgroundWidth = this.background.Width;
            this.backgroundHeight = this.background.Height;
            this.backgrounddebug = content.Load<Texture2D>(backgrounddebug);
            
            this.parallax = content.Load<Texture2D>(parallax);
            this.parallaxWidth = this.parallax.Width;
            this.parallaxHeight = this.parallax.Height;
        }

        //constructor - scrolling background, no parallax
        public Background(ContentManager content, string background)
        {
            this.background = content.Load<Texture2D>(background);
            this.backgroundWidth = this.background.Width;
            this.backgroundHeight = this.background.Height;
            this.parallaxWidth = this.parallax.Width;
            this.parallaxHeight = this.parallax.Height;
            drawParallax = false;
        }


        //draw the background images

        public void Draw(SpriteBatch spriteBatch)
        {
            //draw the background panel, offset by the players location

            /*When we create the destination rectangle, we set the left position to "-1 * iBackgroundOffset", 
             *which results in shifting our image to the left by a number of pixels equal to iBackgroundOffset.
             */
            spriteBatch.Draw(background, new Rectangle((-1 * backgroundOffset), 0, backgroundWidth, viewportHeight),Color.White);

            /*This works great except when we get to the point where drawing the offset image doesn't fill our entire display. 
             * If we don't account for that, we will end up with a partially filled background and then the XNA Blue Window. 
             * This is where the next statement comes in: 
             */

            //if the right edge of the background panel will end, 
            //within the bounds of the display draw a second copy of the background at that location
            if (backgroundOffset > (backgroundWidth - viewportWidth))
            {
                spriteBatch.Draw(background, new Rectangle(((-1 * backgroundOffset) + backgroundWidth), 0, backgroundWidth, viewportHeight), Color.White);
            }

            /* First we check to see if we need to draw a second copy of the image. 
             * If so, we repeat the above draw call except that we modifiy the position of the second destination rectangle 
             * by adding the width of the background image to the call. 
             * This will line the second copy of the image up to start at exactly the point where the first copy ended. We will never need to draw more than two of these images to fill the screen, since the width of the background image is greater than the width of the screen. 
             * The rest of our draw function does exactly the same process with the parallax star overlay after checking to see if we should be drawing it. 
             * It uses the same offsetting and second copy drawing logic as the background. 
             * */

            if (drawParallax)
            {
                //draw the parallax starfield
                spriteBatch.Draw(parallax, new Rectangle((-1 * parallaxOffset), 0, parallaxWidth, viewportHeight), Color.SlateGray);

                //if the player is past the point where the star field will end on the active screen, 
                //we need to draw a second copy of it to cover the remaining screen area
                if(parallaxOffset > (parallaxWidth - viewportWidth))
                {
                    spriteBatch.Draw(parallax, new Rectangle(((-1 * parallaxOffset) + parallaxWidth), 0, parallaxWidth, viewportHeight), Color.SlateGray);
                }

            }

            

        }








    }
}
